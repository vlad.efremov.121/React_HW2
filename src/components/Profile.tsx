import { connect } from "react-redux";

interface Props {
    userName: string;
    isAutorized: boolean;
}

const Profile = (props: Props) => {
    console.log("Profile: props = ", props);
    const { userName, isAutorized } = props;
    
    if(isAutorized) {
        return <div>
            <div>Добро пожаловать, {userName}</div>
        </div>
    }
    else {
        return <div>
            <div>Пользователь не авторизован</div>
        </div>
    }  
}

const mapState = (state: any) => {
    console.log("Profile: state = ", state);
    return {userName: state.user.userName, isAutorized: state.user.isAutorized}
}

export default connect(mapState)(Profile);