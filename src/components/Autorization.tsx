import { useState } from 'react';
import { useDispatch } from "react-redux";
import { Actions } from "../stateManagement/AutorizationReducer";
import Profile from './Profile';
import { Typography, TextField, Button } from '@mui/material';

const AutorizationScr = () => {
    const dispatch = useDispatch();
    const [userName, setName] = useState('');
    const [password, setPass] = useState('');

    const enter = () => {
        const user = {userName: userName, password: password, isAutorized: false};
        console.log("enter function: user = ", user);
        dispatch(Actions.checkUser(user));
    };

    return <div>
        <Typography>Имя пользователя: </Typography>
        <TextField
        name="userName"
        onChange={(event) => setName(event.target.value)}
        />
        
        <Typography>Пароль: </Typography>
        <TextField
        name="password"
        type="password"
        onChange={(event) => setPass(event.target.value)}
        />

        <br/><br/>
        <Button variant="outlined" onClick={enter}>Подтвердить</Button>
        <br/><br/>
        <Profile />
    </div>
}

export default AutorizationScr;