import React from 'react';
import './App.css';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';
import HomePage from './components/HomePage';
import Register from './components/Register';
import Login from './components/Login';
import AutorizationScr from './components/Autorization';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route index element={<HomePage />} />
        <Route path="register" element={<Register />} />
        <Route path="login" element={<Login />} />
        <Route path="autorization">
          <Route index element={<AutorizationScr />} />
        </Route>
        <Route path="*" element={<span>404</span>} />
      </Routes>

      <nav>
        <ul>
          <li>
            <Link to={'/register'}>Регистрация</Link>
          </li>
          <li>
            <Link to={'/autorization'}>Войти</Link>
          </li>
          <li>
            <Link to={'/404'}>404</Link>
          </li>
        </ul>
      </nav>
    </BrowserRouter>
  );
}

export default App;
