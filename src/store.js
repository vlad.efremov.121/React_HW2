import { createStore } from 'redux';
import { AutorizationReducer } from "./stateManagement/AutorizationReducer";

const store = createStore(AutorizationReducer);

export default store;